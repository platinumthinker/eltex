#include "main.h"

int main(int argc, char **argv)
{
	int epollfd, sock_serv, client_serv, count_event;
	struct epoll_event ev, events[MAX_EVENT];
	clients = (SERV*) malloc(sizeof(SERV) * MAX_CLIENT);
	nickname = (char *) malloc(sizeof(char) * MAX_LEN_NICK);
	printf("Enter you nickname:\n");
	nickname = gets(nickname);

	for(int i = 0; i < MAX_CLIENT; i++)
	{
		clients[i].fd = -1;
		strcpy(clients[i].nickname, "");
	}

	signal(SIGINT, close_all);

	if ( (epollfd = epoll_create(MAX_CLIENT) ) == -1 )
	{
		perror("epoll create");
		exit(1);
	}

	//Key read event
	ev.events = EPOLLIN;
	ev.data.fd = STDIN_FILENO;
	if( epoll_ctl(epollfd, EPOLL_CTL_ADD, STDIN_FILENO, &ev) == -1 )
	{
		perror("epoll ctl key");
		exit(1);
	}  
	
	connect(epollfd, argc, argv);
	
	for(;;)
	{
		count_event = epoll_wait(epollfd, events, MAX_EVENT, -1);
		if( count_event == -1)
		{
			perror("epoll_wait");
			//TODO
			exit(1);	
		}
		
		for(int i = 0; i < count_event; i++)
		{
			if( events[i].data.fd == sock_serv )
			{
				//Serv
				printf("Server listen:\n");
				server(epollfd, sock_serv);
			}else if(events[i].data.fd == STDIN_FILENO)
			{
				//Input
				client_in(client_serv);	
			}
			else
			{
				//Msg
				printf("Client Listen: \n");
				if(client_out(events[i].data.fd) == 0)
				{
					//Reconnect	
					reconnect(events[i].data.fd, epollfd);
				}
			}
		}
	}
}
