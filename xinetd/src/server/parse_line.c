#include "main.h"

int parse_line(FILE *file, PROGRAM_PORT *buf, char *str)
{
	if( fscanf(file, "%d: ", &(buf->port)) == EOF )
	{
		return 0;
	}
	
	int i = 0;
	int ch = fgetc(file);
	
	while( ch != EOF && ch != '\n' )
	{
		str[i++] = (char)ch;
		ch = fgetc(file);
		if(i == MAX_CHAR_BUFFER - 1)
		{
			printf("Warning! Split programm name %s", str);
			break;
		}
	}
	
	str[i] = '\0';
	buf->program = malloc(strlen(str) + 1);
	strcpy(buf->program, str);
	
	return (ch == EOF) ? 0 : 1; 
}
