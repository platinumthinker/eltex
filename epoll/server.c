#include "main.h"

int server(int epollfd, int socket)
{
	int conn, addrlen, selID = 0, flag_suc = 0;
	struct sockaddr_in addr;
	struct epoll_event ev;
	char *msg;

	for(selID = 0; selID < MAX_CLIENT; selID++)
		if( clients[selID].fd == -1 ) 
		{
			//Search free place
			flag_suc = 1;
			break;
		}
	
	if( flag_suc )
	{
		if(( clients[selID].fd = accept(socket, &addr, &addrlen)) == -1 )
		{
			perror("accept in server.c");
			return -1;
		}
		
		clients[selID].addr = addr;
		ev.events = EPOLLIN;
		ev.data.fd = clients[selID].fd;
		if( epoll_ctl(epollfd, EPOLL_CTL_ADD, clients[selID].fd, &ev) == -1)
		{
			perror("epoll_ctl in server");
			return -2;
		}
		
		
		read_msg(socket, msg);
		printf("Login new user: %s", msg);
		strcpy(clients[selID].nickname, msg);

		return 0;
	}
	else
	{
		if( (conn = accept(socket, &addr, &addrlen)) == -1 )
		{
			perror("accept in server.c");
			return 1;
		}

		read_msg(socket, msg);
		printf("Redirect new user: %s\n", msg);		

		char a = 255;
		
		write(conn, &a, 1);
		write(conn, &clients[0].addr, sizeof(clients[0].addr));
		close(conn);
	}
}
