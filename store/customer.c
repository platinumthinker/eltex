#include "main.h"

void customer(int *count)
{
    int countShop  = (int) (count[0]);
    int countFood  = (int) (count[1]);
    int numCustmer = (int) (count[2]);
    int needFood = ((double)rand() / RAND_MAX) * countFood + 5;
    printf("%d\n", needFood);
    do
    {
        pthread_mutex_lock(&mtx);
        for(int i = 0; i < countShop; i++)
        {
            if(needFood == 0)
                break;
            if(shops[i])
            {
                needFood -= (shops[i] > needFood) ? needFood : shops[i];
                shops[i] -= (shops[i] > needFood) ? needFood : shops[i];
            }
        }
        printFood(countShop, numCustmer);
        sleep(1);
        pthread_mutex_unlock(&mtx);
    }while(needFood);
}
