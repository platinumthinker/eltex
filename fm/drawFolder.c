#include "main.h"

int drawFolder(char *name, int *num, char **selectfile)
{
        struct stat st;
        char **names;
        int res = 0;
        int fileCount;
        if(!file_in_folder(name, &names, &fileCount))
        {
            if(names != NULL) free(names);
            return 0;
        }
        if(fileCount == 3)
            *num = 0;
        else
        if(fileCount - 2 <= *num)
            (*num) %= fileCount - 3, (*num)--;
        if(*num < 0) 
            *num = fileCount - 3;
        for(int i = 2; i < fileCount; i++)
        {
            char buf[strlen(names[i]) + strlen(name) + 1];
            strcpy(buf, name);
            strcat(buf, "/");
            strcat(buf, names[i]);
            stat(buf, &st);
            if(*num == i - 2)
            {
                    printf("->");
                    (*selectfile) = (char*)malloc(strlen(names[i]) + 1);
                    strcpy((*selectfile), names[i]);
                    if(S_ISDIR(st.st_mode))
                    {    
                        printf("*%s\n", names[i]);
						res = 1;
                        continue;
                    }
		    		else
		    		{
						res = 0;
		    		}
            }
            else
                    printf("  ");
            if(S_ISDIR(st.st_mode))
                    printf("*");
            else printf(" ");
            printf("%s\n", names[i]);
            free(names[i]);
        }
        free(names);
        return res;
}
