#include "main.h"

int comp(const void *a, const void *b)
{
        return strcmp(*(char * const *)a, *(char * const *)b) ;
}

int file_in_folder(char *folder_name, char ***filesname, int *countfiles)
{
        DIR* dir = opendir(folder_name);
        (*countfiles) = 0;
        if(dir == NULL)
                return 0;
        struct dirent *entry;
        while((entry = readdir(dir)) != NULL)
                (*countfiles)++;
        rewinddir(dir);
        *filesname = (char **) malloc(sizeof(char*) * (*countfiles));
        for(int i = 0; i < (*countfiles), (entry = readdir(dir)) != NULL; i++)
        {
                (*filesname)[i] = (char*) malloc(strlen(entry->d_name));
                strcpy((*filesname)[i], entry->d_name);
        }
        closedir(dir);
        qsort(*filesname, *countfiles, sizeof(char*), &comp);

        return 1;
}
