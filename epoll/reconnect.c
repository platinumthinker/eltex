#include "main.h"

int reconnect(int socket, int epollfd)
{	
	struct sockaddr_in addr;
	if( read(socket, &addr, sizeof(addr)) < 0 )
	{
		perror("read in reconnect");
		return -1;
	}
	
	int sock = client_socket_in(addr);
	struct epoll_event ev;
	if( epoll_ctl(epollfd, EPOLL_CTL_DEL, socket, &ev) == -1)
	{
		perror("epoll ctl del in client");
		return -1;
	}
	
	ev.events = EPOLLIN;
	ev.data.fd = sock;
	if( epoll_ctl(epollfd, EPOLL_CTL_ADD, sock, &ev) == -1)
	{
		perror("epoll ctl add in client");
		return -2;
	}
	
	return 0;
}
