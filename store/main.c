#include "main.h"

int main(int argc, char **argv)
{
    if(argc == 4)
    {
        int countShop = atoi(argv[1]);
        int countCustomer = atoi(argv[2]);
        int countFood = atoi(argv[3]);
        srand(time(NULL));
        shops = (int *) malloc( sizeof(int) * countShop);
        for(int i = 0; i < countShop; i++)
            shops[i] = ((double) rand() / RAND_MAX) * countFood;
        
        pthread_mutex_init(&mtx, NULL);
        pthread_t customers[countCustomer];
        for(int i = 0; i < countCustomer; i++)
        {
             int arg[3] = {countShop, countFood, i + 1};
             pthread_create(&customers[i], NULL, customer, arg); 
        }

        do
        {
            pthread_mutex_lock(&mtx);
            for(int i = 0; i < countShop; i++)
            {
                if(shops[i] == 0)
                    shops[i] = ((double) rand() / RAND_MAX) * countFood;
            }
            printFood(countShop, 0);
            pthread_mutex_unlock(&mtx);
            sleep(2);
        }while(1);
    }
    else
        return 0;
}
