#include "main.h"

int main(int argc, char **argv)
{
	char *str = (char*)malloc(sizeof(char) * 1);
	int sockfd = client_socket_init(argv[1], argv[2]);
	if( sockfd < 0 )
	{
		free(str);
		exit(EXIT_FAILURE);
	}
		
	int epollfd = epoll_init(sockfd);
		
	if( epollfd < 0)
	{
		free(str);
		exit(EXIT_FAILURE);
	}

	struct epoll_event events[MAX_EVENTS];
	struct termios term, def;
	int count_events;
	
	tcgetattr(STDIN_FILENO, &def);
	tcgetattr(STDIN_FILENO, &term);
	term.c_cflag &= ~ICANON;
	term.c_cflag &= ~ECHO;
	term.c_cflag &= ~ISIG;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);

	for(;;)
	{
		count_events = epoll_wait(epollfd, events, MAX_EVENTS, -1);
		if( count_events == -1 )
		{
			perror("Error epoll wait");
			free(str);
			exit(EXIT_FAILURE);
		}
		printf("Count ev: %d\n", count_events);
		for(int i = 0; i < count_events; i++) 
		{
			if( events[i].data.fd == sockfd )
			{
				if(read(sockfd, str, 1) == EOF)
				{
					printf("EOF\n");
				}
				write(STDOUT_FILENO, str, 1);		
			}
			if( (events[i].data.fd == STDIN_FILENO) 
				&& (events[i].events & EPOLLIN ))
			{
				read(STDIN_FILENO, str, 1);
				write(sockfd, str, 1);
			}
		}
	}
	return EXIT_SUCCESS;
}
