#include "main.h"

int read_msg(int socket, char* msg)
{
	int i = 0;
	int len = 0;
	char buf; msg = NULL;
    do
    {
   		if(read(socket, &buf, 1) <= 0)
			return -1;
		
   		if(i == 0 && buf == 255)
   		{
   			write(STDOUT_FILENO, "Reconnect to \
   				new server!\n", 26);
   			return 0; //if reconnect
   		}
		
		if( i == len )		
		{
			len += 20;
			msg = (char*) realloc(msg, sizeof(char)*len);
		}
		msg[i++] = buf;
    }while(len > 0 && buf != '\0' );
	i++;	
	msg = (char*) realloc(msg, sizeof(char) * i);
	return 1;
}
