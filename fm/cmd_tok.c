#include "main.h"

int cmd_tok(char *cmd, char ***tok, int *count)
{
	int i, j, len, index;
	char *buf = malloc(strlen(cmd) + 1);
	for(index = 1, i = 0; cmd[i] != '\0'; i++)
	{
		if(cmd[i] == '|' && i > 0 && cmd[i - 1] != '|')
			index++;
	}
	
	(*tok) = (char**) malloc(sizeof(char *) * index);
	*count = index;
	
	for(i = 0, j = 0, len = 1, index = 0; cmd[i] != '\0'; i++, len++) 
	{
		if( cmd[i] == '|' )
		{
			if( i > 0 && cmd[i - 1] != '|' )
			{
				(*tok)[index] = strndup( &cmd[j], len + 1 );
				(*tok)[index][len - 1] = '\0';
				index++;
			}
			j = i + 1;
			len = 0;
		}
	}
	
	(*tok)[index] = strndup( &cmd[j], len + 1 );
	(*tok)[index][len - 1] = '\0';
	index++;
	
	return 0;
}
