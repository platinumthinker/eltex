#include "main.h"

int socket_tcp_init(int port)
{
	struct sockaddr_in addr;
	int sockfd = 0;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if( sockfd < 0 )
	{
		perror("don't create socket");
		return -1;
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if( bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0 )
	{
		perror("don't bind socket");
		return -1;
	}

	if( listen(sockfd, 20) < 0 )
	{
		perror("listen");
		return -1;
	}
	return sockfd;
}
