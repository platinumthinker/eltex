#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/epoll.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#define DEFAULT_FILE_CONF "conf"
#define MAX_EVENT 16
#define MAX_CHAR_BUFFER 400
#define MAX_STD_DESCRIPTOR 2

typedef struct 
{
	char *program;
	int port;
	int socket;
} PROGRAM_PORT;
PROGRAM_PORT *programs;

int programs_count;

void printHelp();
int openConfigureFile(const char *filepath);
int parse_line(FILE *f, PROGRAM_PORT *b, char *str);
int epoll_init();
int socket_tcp_init(int port);
void server(int epollfd);
int find_index_program(int);
void thread_prog(int , int);
