#include "main.h"

int connect(int epollfd, int argc, char **argv)
{
	//Connect
	if(argc > 2)
	{
		sock_serv = server_socket_init(argv[3]);
		client_serv = client_socket_init(argv[1], argv[2]);
				
		ev.events = EPOLLIN;
		ev.data.fd = client_serv;
		if( epoll_ctl(epollfd, EPOLL_CTL_ADD, client_serv, &ev) == -1 )
		{
			perror("epoll ctl client");
			exit(1);
		}
	}
	else
	{
		sock_serv = server_socket_init(argv[1]);
	}
	ev.events = EPOLLIN;
	ev.data.fd = sock_serv;
	if( epoll_ctl(epollfd, EPOLL_CTL_ADD, sock_serv, &ev) == -1 )
	{
		perror("epoll ctl server");
		exit(1);
	}
}
