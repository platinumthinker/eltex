#include "main.h"

void close_all(int sig)
{
	char a = 255;
	for(int i = 0; i < MAX_CLIENT; i++)
	{
		if( clients[i].fd != -1)
		{
			write(clients[i].fd, &a, 1); 
			write(clients[i].fd, &clients[i].addr, sizeof(clients[i].addr));
			close(clients[i].fd);
		}
	}
}
