#include "main.h"

int epoll_init()
{
	struct epoll_event ev;
	int epollfd;
	if(( epollfd = epoll_create(MAX_EVENT))  == -1 )
	{
		perror("epoll create");
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < programs_count; i++) 
	{
		ev.data.fd = socket_tcp_init( programs[i].port );
		printf("Socket init: %s - %d\n", programs[i].program, ev.data.fd);
		if(ev.data.fd == -1)
		{
			continue;
		}
		
		printf("Socket init %d\n", programs[i].port);
		programs[i].socket = ev.data.fd;
		ev.events = EPOLLIN;
		if( epoll_ctl(epollfd, EPOLL_CTL_ADD, ev.data.fd, &ev ) == -1 )
		{
			perror("Epoll_ctl add in epoll_init");
			exit(EXIT_FAILURE);
		}
	}

	return epollfd;
}
