	.file	"search.c"
	.text
.Ltext0:
	.globl	c_array_size
	.section	.rodata
	.align 8
	.type	c_array_size, @object
	.size	c_array_size, 8
c_array_size:
	.quad	100000000
	.text
	.type	generate_row, @function
generate_row:
.LFB0:
	.file 1 "./search.c"
	.loc 1 21 0
	.cfi_startproc
	pushq	%rbp
.LCFI0:
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
.LCFI1:
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	call	mcount
	.loc 1 23 0
	call	rand
	movl	%eax, %ecx
	movl	$1374389535, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$5, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	imull	$100, %eax, %eax
	movl	%ecx, %esi
	subl	%eax, %esi
	movl	%esi, %eax
	andl	$127, %eax
	movzbl	%al, %eax
	andl	$127, %eax
	movl	%eax, %edx
	sall	$5, %edx
	movzwl	-30(%rbp), %eax
	andw	$-4065, %ax
	orl	%edx, %eax
	movw	%ax, -30(%rbp)
	.loc 1 24 0
	call	rand
	movl	%eax, %ecx
	movl	$274877907, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$6, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	imull	$1000, %ebx, %eax
	movl	%ecx, %ebx
	subl	%eax, %ebx
	call	rand
	movl	%eax, %ecx
	movl	$274877907, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$6, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %esi
	subl	%eax, %esi
	movl	%esi, %eax
	imull	$1000, %eax, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	imull	%ebx, %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	andl	$1048575, %edx
	movl	-28(%rbp), %eax
	andl	$-1048576, %eax
	orl	%edx, %eax
	movl	%eax, -28(%rbp)
	.loc 1 25 0
	call	rand
	movl	%eax, %ecx
	movl	$274877907, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$6, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	imull	$1000, %ebx, %eax
	movl	%ecx, %ebx
	subl	%eax, %ebx
	call	rand
	movl	%eax, %ecx
	movl	$274877907, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$6, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %esi
	subl	%eax, %esi
	movl	%esi, %eax
	imull	$1000, %eax, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	imull	%ebx, %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	andl	$1048575, %edx
	movl	-32(%rbp), %eax
	andl	$-1048576, %eax
	orl	%edx, %eax
	movl	%eax, -32(%rbp)
	.loc 1 26 0
	call	rand
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	andl	$1, %eax
	subl	%edx, %eax
	andl	$1, %eax
	andl	$1, %eax
	movl	%eax, %edx
	sall	$4, %edx
	movzbl	-30(%rbp), %eax
	andl	$-17, %eax
	orl	%edx, %eax
	movb	%al, -30(%rbp)
	.loc 1 27 0
	call	rand
	movl	%eax, %ecx
	movl	$458129845, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$5, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	imull	$300, %eax, %eax
	movl	%ecx, %esi
	subl	%eax, %esi
	movl	%esi, %eax
	andw	$511, %ax
	andw	$511, %ax
	movl	%eax, %edx
	sall	$4, %edx
	movzwl	-26(%rbp), %eax
	andw	$-8177, %ax
	orl	%edx, %eax
	movw	%ax, -26(%rbp)
	.loc 1 28 0
	movq	-32(%rbp), %rax
	.loc 1 29 0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
.LCFI2:
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	generate_row, .-generate_row
	.type	test_predicate, @function
test_predicate:
.LFB1:
	.loc 1 43 0
	.cfi_startproc
	pushq	%rbp
.LCFI3:
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
.LCFI4:
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	mcount
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	.loc 1 45 0
	movq	-16(%rbp), %rax
	movzbl	16(%rax), %eax
	.loc 1 53 0
	testb	%al, %al
	je	.L4
	.loc 1 46 0
	movq	-8(%rbp), %rax
	movl	4(%rax), %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	4(%rax), %eax
	andl	$1048575, %eax
	.loc 1 45 0
	cmpl	%eax, %edx
	jl	.L5
	.loc 1 47 0
	movq	-8(%rbp), %rax
	movl	4(%rax), %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	12(%rax), %eax
	andl	$1048575, %eax
	.loc 1 46 0
	cmpl	%eax, %edx
	jg	.L5
.L4:
	.loc 1 48 0
	movq	-16(%rbp), %rax
	movzbl	17(%rax), %eax
	.loc 1 47 0
	testb	%al, %al
	je	.L6
	.loc 1 49 0
	movq	-8(%rbp), %rax
	movzbl	2(%rax), %eax
	shrb	$4, %al
	andl	$1, %eax
	movzbl	%al, %edx
	movq	-16(%rbp), %rax
	movzbl	2(%rax), %eax
	shrb	$4, %al
	andl	$1, %eax
	movzbl	%al, %eax
	.loc 1 48 0
	cmpl	%eax, %edx
	jl	.L5
	.loc 1 49 0
	movq	-8(%rbp), %rax
	movzbl	2(%rax), %eax
	shrb	$4, %al
	andl	$1, %eax
	movzbl	%al, %edx
	movq	-16(%rbp), %rax
	movzbl	10(%rax), %eax
	shrb	$4, %al
	andl	$1, %eax
	movzbl	%al, %eax
	cmpl	%eax, %edx
	jg	.L5
.L6:
	.loc 1 50 0 discriminator 1
	movq	-16(%rbp), %rax
	movzbl	18(%rax), %eax
	.loc 1 49 0 discriminator 1
	testb	%al, %al
	je	.L7
	.loc 1 51 0
	movq	-8(%rbp), %rax
	movzwl	2(%rax), %eax
	shrw	$5, %ax
	andl	$127, %eax
	movzbl	%al, %edx
	movq	-16(%rbp), %rax
	movzwl	2(%rax), %eax
	shrw	$5, %ax
	andl	$127, %eax
	movzbl	%al, %eax
	.loc 1 50 0
	cmpl	%eax, %edx
	jl	.L5
	.loc 1 51 0
	movq	-8(%rbp), %rax
	movzwl	2(%rax), %eax
	shrw	$5, %ax
	andl	$127, %eax
	movzbl	%al, %edx
	movq	-16(%rbp), %rax
	movzwl	10(%rax), %eax
	shrw	$5, %ax
	andl	$127, %eax
	movzbl	%al, %eax
	cmpl	%eax, %edx
	jg	.L5
.L7:
	.loc 1 52 0 discriminator 1
	movq	-16(%rbp), %rax
	movzbl	19(%rax), %eax
	.loc 1 51 0 discriminator 1
	testb	%al, %al
	je	.L8
	.loc 1 53 0
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	andl	$1048575, %eax
	.loc 1 52 0
	cmpl	%eax, %edx
	jl	.L5
	.loc 1 53 0
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	andl	$1048575, %eax
	cmpl	%eax, %edx
	jg	.L5
.L8:
	.loc 1 54 0 discriminator 1
	movq	-16(%rbp), %rax
	movzbl	20(%rax), %eax
	.loc 1 53 0 discriminator 1
	testb	%al, %al
	je	.L9
	.loc 1 55 0
	movq	-8(%rbp), %rax
	movzwl	6(%rax), %eax
	shrw	$4, %ax
	andw	$511, %ax
	movzwl	%ax, %edx
	movq	-16(%rbp), %rax
	movzwl	6(%rax), %eax
	shrw	$4, %ax
	andw	$511, %ax
	movzwl	%ax, %eax
	.loc 1 54 0
	cmpl	%eax, %edx
	jl	.L5
	.loc 1 55 0
	movq	-8(%rbp), %rax
	movzwl	6(%rax), %eax
	shrw	$4, %ax
	andw	$511, %ax
	movzwl	%ax, %edx
	movq	-16(%rbp), %rax
	movzwl	14(%rax), %eax
	shrw	$4, %ax
	andw	$511, %ax
	movzwl	%ax, %eax
	cmpl	%eax, %edx
	jg	.L5
.L9:
	.loc 1 53 0 discriminator 1
	movl	$1, %eax
	jmp	.L10
.L5:
	.loc 1 53 0 is_stmt 0 discriminator 2
	movl	$0, %eax
.L10:
	.loc 1 56 0 is_stmt 1
	leave
.LCFI5:
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	test_predicate, .-test_predicate
	.type	search, @function
search:
.LFB2:
	.loc 1 62 0
	.cfi_startproc
	pushq	%rbp
.LCFI6:
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
.LCFI7:
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	call	mcount
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	.loc 1 63 0
	movq	$0, -8(%rbp)
	.loc 1 65 0
	movq	$0, -16(%rbp)
	jmp	.L13
.L15:
	.loc 1 66 0
	movq	-16(%rbp), %rax
	leaq	0(,%rax,8), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	test_predicate
	testb	%al, %al
	je	.L14
	.loc 1 67 0
	movq	-8(%rbp), %rax
	leaq	0(,%rax,8), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movq	-16(%rbp), %rax
	leaq	0(,%rax,8), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	addq	$1, -8(%rbp)
.L14:
	.loc 1 65 0
	addq	$1, -16(%rbp)
.L13:
	.loc 1 65 0 is_stmt 0 discriminator 1
	movq	-16(%rbp), %rax
	cmpq	-32(%rbp), %rax
	jb	.L15
	.loc 1 69 0 is_stmt 1
	movq	-8(%rbp), %rax
	.loc 1 70 0
	leave
.LCFI8:
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	search, .-search
	.section	.rodata
.LC0:
	.string	"calloc error"
.LC1:
	.string	"Generated rows: %d \n"
.LC2:
	.string	"C-Searching..."
.LC4:
	.string	"C-search took %f seconds.\n"
.LC5:
	.string	"Found rows: %d \n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB3:
	.loc 1 73 0
	.cfi_startproc
	pushq	%rbp
.LCFI9:
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
.LCFI10:
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -24
	call	mcount
	.loc 1 76 0
	movq	c_array_size(%rip), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	call	calloc
	movq	%rax, -32(%rbp)
	.loc 1 77 0
	cmpq	$0, -32(%rbp)
	jne	.L18
	.loc 1 78 0
	movl	$.LC0, %edi
	call	puts
	.loc 1 79 0
	movl	$1, %edi
	call	exit
.L18:
	.loc 1 86 0
	movq	$0, -24(%rbp)
	jmp	.L19
.L20:
	.loc 1 87 0 discriminator 2
	movq	-24(%rbp), %rax
	leaq	0(,%rax,8), %rdx
	movq	-32(%rbp), %rax
	leaq	(%rdx,%rax), %rbx
	movl	$0, %eax
	call	generate_row
	movq	%rax, (%rbx)
	.loc 1 86 0 discriminator 2
	addq	$1, -24(%rbp)
.L19:
	.loc 1 86 0 is_stmt 0 discriminator 1
	movq	c_array_size(%rip), %rax
	cmpq	%rax, -24(%rbp)
	jb	.L20
	.loc 1 88 0 is_stmt 1
	movq	c_array_size(%rip), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	.loc 1 90 0
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	.loc 1 92 0
	call	rand
	movb	$0, -80(%rbp)
	.loc 1 93 0
	call	rand
	movb	$0, -79(%rbp)
	.loc 1 94 0
	call	rand
	movb	$0, -76(%rbp)
	.loc 1 96 0
	call	rand
	movl	%eax, %ecx
	movl	$1374389535, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$5, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	imull	$100, %eax, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	andl	$127, %eax
	movzbl	%al, %eax
	andl	$127, %eax
	movl	%eax, %edx
	sall	$5, %edx
	movzwl	-94(%rbp), %eax
	andw	$-4065, %ax
	orl	%edx, %eax
	movw	%ax, -94(%rbp)
	.loc 1 97 0
	movzwl	-94(%rbp), %eax
	shrw	$5, %ax
	andl	$127, %eax
	addl	$5, %eax
	andl	$127, %eax
	movzbl	%al, %eax
	andl	$127, %eax
	movl	%eax, %edx
	sall	$5, %edx
	movzwl	-86(%rbp), %eax
	andw	$-4065, %ax
	orl	%edx, %eax
	movw	%ax, -86(%rbp)
	.loc 1 98 0
	call	rand
	movb	$1, -78(%rbp)
	.loc 1 100 0
	call	rand
	movl	%eax, %ecx
	movl	$1172812403, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$13, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	imull	$30000, %eax, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	andl	$1048575, %edx
	movl	-96(%rbp), %eax
	andl	$-1048576, %eax
	orl	%edx, %eax
	movl	%eax, -96(%rbp)
	.loc 1 101 0
	movl	-96(%rbp), %eax
	andl	$1048575, %eax
	addl	$5, %eax
	andl	$1048575, %eax
	movl	%eax, %edx
	andl	$1048575, %edx
	movl	-88(%rbp), %eax
	andl	$-1048576, %eax
	orl	%edx, %eax
	movl	%eax, -88(%rbp)
	.loc 1 102 0
	call	rand
	movb	$1, -77(%rbp)
	.loc 1 104 0
	movq	c_array_size(%rip), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	call	calloc
	movq	%rax, -40(%rbp)
	.loc 1 109 0
	movl	$.LC2, %edi
	call	puts
	.loc 1 111 0
	call	clock
	movq	%rax, -48(%rbp)
	.loc 1 112 0
	movq	c_array_size(%rip), %rsi
	leaq	-96(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	search
	movq	%rax, -56(%rbp)
	.loc 1 113 0
	call	clock
	movq	%rax, -64(%rbp)
	.loc 1 114 0
	movq	-48(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	cvtsi2ssq	%rax, %xmm0
	movss	%xmm0, -68(%rbp)
	.loc 1 115 0
	movss	-68(%rbp), %xmm0
	movss	.LC3(%rip), %xmm1
	divss	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movl	$.LC4, %edi
	movl	$1, %eax
	call	printf
	.loc 1 117 0
	movq	-56(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	.loc 1 124 0
	movl	$0, %eax
	.loc 1 125 0
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
.LCFI11:
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC3:
	.long	1232348160
	.text
.Letext0:
	.file 2 "/usr/lib/gcc/x86_64-linux-gnu/4.7/include/stddef.h"
	.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 4 "/usr/include/time.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x34b
	.value	0x2
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF39
	.byte	0x1
	.long	.LASF40
	.long	.LASF41
	.quad	.Ltext0
	.quad	.Letext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF8
	.byte	0x2
	.byte	0xd5
	.long	0x38
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.long	.LASF1
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.long	.LASF2
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.long	.LASF3
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF6
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF7
	.uleb128 0x2
	.long	.LASF9
	.byte	0x3
	.byte	0x91
	.long	0x69
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF10
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF11
	.uleb128 0x2
	.long	.LASF12
	.byte	0x4
	.byte	0x3c
	.long	0x77
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF13
	.uleb128 0x5
	.long	.LASF42
	.byte	0x4
	.byte	0x1
	.byte	0x8
	.long	0xd3
	.uleb128 0x6
	.long	.LASF14
	.sleb128 0
	.uleb128 0x6
	.long	.LASF15
	.sleb128 1
	.uleb128 0x6
	.long	.LASF16
	.sleb128 2
	.uleb128 0x6
	.long	.LASF17
	.sleb128 3
	.uleb128 0x6
	.long	.LASF18
	.sleb128 4
	.uleb128 0x6
	.long	.LASF19
	.sleb128 5
	.byte	0
	.uleb128 0x7
	.long	.LASF24
	.byte	0x8
	.byte	0x1
	.byte	0xb
	.long	0x135
	.uleb128 0x8
	.long	.LASF20
	.byte	0x1
	.byte	0xc
	.long	0x4d
	.byte	0x4
	.byte	0x14
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.long	.LASF21
	.byte	0x1
	.byte	0xd
	.long	0x4d
	.byte	0x4
	.byte	0x1
	.byte	0xb
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x9
	.string	"age"
	.byte	0x1
	.byte	0xe
	.long	0x4d
	.byte	0x4
	.byte	0x7
	.byte	0x4
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.long	.LASF22
	.byte	0x1
	.byte	0xf
	.long	0x4d
	.byte	0x4
	.byte	0x14
	.byte	0xc
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.long	.LASF23
	.byte	0x1
	.byte	0x10
	.long	0x4d
	.byte	0x4
	.byte	0x9
	.byte	0x3
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0x7
	.long	.LASF25
	.byte	0x18
	.byte	0x1
	.byte	0x21
	.long	0x16c
	.uleb128 0xa
	.long	.LASF26
	.byte	0x1
	.byte	0x22
	.long	0xd3
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.string	"end"
	.byte	0x1
	.byte	0x22
	.long	0xd3
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xa
	.long	.LASF27
	.byte	0x1
	.byte	0x24
	.long	0x16c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0xc
	.long	0x3f
	.long	0x17c
	.uleb128 0xd
	.long	0x70
	.byte	0x4
	.byte	0
	.uleb128 0xe
	.long	.LASF43
	.byte	0x1
	.byte	0x15
	.long	0xd3
	.quad	.LFB0
	.quad	.LFE0
	.long	.LLST0
	.byte	0x1
	.long	0x1af
	.uleb128 0xf
	.long	.LASF34
	.byte	0x1
	.byte	0x16
	.long	0xd3
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x10
	.long	.LASF29
	.byte	0x1
	.byte	0x29
	.byte	0x1
	.long	0x3f
	.quad	.LFB1
	.quad	.LFE1
	.long	.LLST1
	.byte	0x1
	.long	0x1f1
	.uleb128 0x11
	.string	"row"
	.byte	0x1
	.byte	0x29
	.long	0x1f1
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x12
	.long	.LASF28
	.byte	0x1
	.byte	0x2a
	.long	0x201
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.uleb128 0x13
	.long	0x1f6
	.uleb128 0x14
	.byte	0x8
	.long	0x1fc
	.uleb128 0x13
	.long	0xd3
	.uleb128 0x13
	.long	0x206
	.uleb128 0x14
	.byte	0x8
	.long	0x20c
	.uleb128 0x13
	.long	0x135
	.uleb128 0x15
	.long	.LASF30
	.byte	0x1
	.byte	0x3c
	.byte	0x1
	.long	0x2d
	.quad	.LFB2
	.quad	.LFE2
	.long	.LLST2
	.byte	0x1
	.long	0x289
	.uleb128 0x12
	.long	.LASF31
	.byte	0x1
	.byte	0x3c
	.long	0x1f1
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x12
	.long	.LASF32
	.byte	0x1
	.byte	0x3c
	.long	0x289
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x12
	.long	.LASF33
	.byte	0x1
	.byte	0x3d
	.long	0x28e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x12
	.long	.LASF28
	.byte	0x1
	.byte	0x3d
	.long	0x201
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0xf
	.long	.LASF35
	.byte	0x1
	.byte	0x3f
	.long	0x2d
	.byte	0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.string	"i"
	.byte	0x1
	.byte	0x40
	.long	0x2d
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.uleb128 0x13
	.long	0x2d
	.uleb128 0x13
	.long	0x293
	.uleb128 0x14
	.byte	0x8
	.long	0xd3
	.uleb128 0x17
	.byte	0x1
	.long	.LASF44
	.byte	0x1
	.byte	0x49
	.long	0x62
	.quad	.LFB3
	.quad	.LFE3
	.long	.LLST3
	.byte	0x1
	.long	0x331
	.uleb128 0x16
	.string	"i"
	.byte	0x1
	.byte	0x4b
	.long	0x2d
	.byte	0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0xf
	.long	.LASF31
	.byte	0x1
	.byte	0x4c
	.long	0x28e
	.byte	0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0xf
	.long	.LASF28
	.byte	0x1
	.byte	0x5a
	.long	0x135
	.byte	0x3
	.byte	0x91
	.sleb128 -112
	.uleb128 0xf
	.long	.LASF33
	.byte	0x1
	.byte	0x68
	.long	0x28e
	.byte	0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0xf
	.long	.LASF35
	.byte	0x1
	.byte	0x6a
	.long	0x2d
	.byte	0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x16
	.string	"end"
	.byte	0x1
	.byte	0x6b
	.long	0x90
	.byte	0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0xf
	.long	.LASF36
	.byte	0x1
	.byte	0x6b
	.long	0x90
	.byte	0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0xf
	.long	.LASF37
	.byte	0x1
	.byte	0x72
	.long	0x331
	.byte	0x3
	.byte	0x91
	.sleb128 -84
	.byte	0
	.uleb128 0x3
	.byte	0x4
	.byte	0x4
	.long	.LASF38
	.uleb128 0x18
	.long	.LASF32
	.byte	0x1
	.byte	0x5
	.long	0x289
	.byte	0x1
	.byte	0x9
	.byte	0x3
	.quad	c_array_size
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x2116
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x2116
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x2116
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.quad	.LFB0-.Ltext0
	.quad	.LCFI0-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI0-.Ltext0
	.quad	.LCFI1-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI1-.Ltext0
	.quad	.LCFI2-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 16
	.quad	.LCFI2-.Ltext0
	.quad	.LFE0-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST1:
	.quad	.LFB1-.Ltext0
	.quad	.LCFI3-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI3-.Ltext0
	.quad	.LCFI4-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI4-.Ltext0
	.quad	.LCFI5-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 16
	.quad	.LCFI5-.Ltext0
	.quad	.LFE1-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST2:
	.quad	.LFB2-.Ltext0
	.quad	.LCFI6-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI6-.Ltext0
	.quad	.LCFI7-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI7-.Ltext0
	.quad	.LCFI8-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 16
	.quad	.LCFI8-.Ltext0
	.quad	.LFE2-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST3:
	.quad	.LFB3-.Ltext0
	.quad	.LCFI9-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI9-.Ltext0
	.quad	.LCFI10-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI10-.Ltext0
	.quad	.LCFI11-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 16
	.quad	.LCFI11-.Ltext0
	.quad	.LFE3-.Ltext0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF42:
	.string	"T_field_enum"
.LASF28:
	.string	"range_filters"
.LASF30:
	.string	"search"
.LASF8:
	.string	"size_t"
.LASF43:
	.string	"generate_row"
.LASF31:
	.string	"array_ptr"
.LASF38:
	.string	"float"
.LASF40:
	.string	"./search.c"
.LASF36:
	.string	"start"
.LASF25:
	.string	"T_range_filters"
.LASF3:
	.string	"unsigned int"
.LASF34:
	.string	"cash_account_row"
.LASF26:
	.string	"begin"
.LASF27:
	.string	"use_filter"
.LASF12:
	.string	"clock_t"
.LASF29:
	.string	"test_predicate"
.LASF1:
	.string	"unsigned char"
.LASF18:
	.string	"height_e"
.LASF16:
	.string	"age_e"
.LASF0:
	.string	"long unsigned int"
.LASF2:
	.string	"short unsigned int"
.LASF23:
	.string	"height"
.LASF19:
	.string	"last_e"
.LASF44:
	.string	"main"
.LASF20:
	.string	"code"
.LASF41:
	.string	"/home/thinker/develop/eltex/GPROF/search"
.LASF21:
	.string	"gender"
.LASF13:
	.string	"long long unsigned int"
.LASF24:
	.string	"T_cash_account_row"
.LASF37:
	.string	"c_took_time"
.LASF17:
	.string	"code_e"
.LASF9:
	.string	"__clock_t"
.LASF7:
	.string	"sizetype"
.LASF11:
	.string	"long long int"
.LASF10:
	.string	"char"
.LASF39:
	.string	"GNU C 4.7.2"
.LASF5:
	.string	"short int"
.LASF32:
	.string	"c_array_size"
.LASF14:
	.string	"amount_of_money_e"
.LASF35:
	.string	"result_size"
.LASF6:
	.string	"long int"
.LASF33:
	.string	"result_ptr"
.LASF4:
	.string	"signed char"
.LASF22:
	.string	"amount_of_money"
.LASF15:
	.string	"gender_e"
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
