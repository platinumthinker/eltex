#include "main.h"

void press_enter(char *filename)
{
	char **tok;
	char *cmd = (char*) malloc(200);
	char *buffer = (char*) malloc(200);
	char *which = "which ";
	int countCmd;
	printf("> ");
	tcsetattr(0, TCSADRAIN, &term);
	
	cmd = gets(cmd);
	cmd_tok(cmd, &tok, &countCmd);
	
	FILE **pipes = (FILE**) malloc (sizeof(FILE*) * countCmd);

	setupConsole();	
	for (int i = 0; i < countCmd; i++) 
	{
		char *buf = (char *) malloc ( strlen(tok[i]) + strlen(which) );
		strcpy(buf, which);
		strcat(buf, tok[i]);
  	
		FILE* file = popen(buf, "r");
		free(buf);

		if( file == NULL )
		{
			perror("popen");
			exit(1);
		}
		
		if( fscanf(file, "%200s", buffer) == -1)
		{
			printf("%s don't found\n", tok[i]);
			getchar();
			free(buffer);
			free(cmd);
			pclose(file);
			return;
		}
  		
		pclose(file);
	}
		
	system("reset");
	
	for (int i = 1; i < countCmd; i++) 
	{
		if( (pipes[i - 1] = popen(tok[i - 1], "r")) == NULL )
		{
			i -= 2;
			while(i > -1)
				pclose(pipes[i--]);
			perror("Error pipes create");
			return;
		}

		if( (pipes[i - 1] = popen(tok[i - 1], "w")) == NULL )
		{
			i--;
			while(i > -1)
				pclose(pipes[i--]);
			perror("Error pipes create");
			return;
		}
	}
	getchar();	
//	dup2(STDIN_FILENO,  pipes[0]);
//	dup2(STDOUT_FILENO, pipes[countCmd - 1]);
	/*
	switch(fork())
	{
		case 0:
			execl("vim", "vim", filename, NULL);
			break;
		default:
			wait();			
			break;
	}
	*/
	free(buffer);
	free(cmd);
}
