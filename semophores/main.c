#inclide "main.h"

int main()
{
	struct sembuf lock = { 0, -1, 0 };
	struct sembuf unlock = {
	int sem_id = semget(KEY, 1, 0644 | IPC_CREAT );
	if( sem_id == -1)
	{
		perror("Error create semaphores");
		exit(1);
	}
//	semctl(sem_id, , );
	
	int pid;
	for(int i = 0; i < FORK_COUNT; i++ ){
		pid = fork();
		if( pid == -1 ){
			perror("Fork, go get to work!");
		}

		if( pid == 0 )
		{
			thread(sem_id);
			break;
		}
	}

	return 0;
}
