#include "main.h"

void thread_prog(int descriptor, int index)
{
	printf("start program: %s\n", programs[index].program);
	fflush(NULL);
	write(descriptor, "Hi!", 4);
	if(fork())
	{
		return;
	}
	else
	{
		for( int fd = 0; fd < MAX_STD_DESCRIPTOR; fd++ )
		{
			if( dup2(descriptor, fd) == -1 )
			{
				exit(EXIT_FAILURE);
			}
		}

		execl(programs[index].program, "", NULL);
	}
}
