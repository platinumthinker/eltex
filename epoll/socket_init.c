#include "main.h"

int client_socket_init(char *ip, char *port)
{
	int sock = 0;
	struct sockaddr_in addr;
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0)
	{
		perror("socket");
		return -1;
	}
	
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(port));
	inet_pton(AF_INET, ip, &addr.sin_addr); 
	if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("connect");
		return -2;
	}
	
	write(sock, nickname, strlen(nickname) + 1);	
	return sock;
}

int client_socket_in(struct sockaddr_in addr)
{
	int sock = 0;
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0)
	{
		perror("socket");
		return -1; } if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("connect");
		return -2;
	}
	
	write(sock, nickname, strlen(nickname) + 1);	
	return sock;
}

int server_socket_init(char *port)
{
	int sock = 0;
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(struct sockaddr));
	
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if( sock < 0 )
	{
		perror("socket");
		return -1;
	}
	
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(port));
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if( bind(sock, &addr, sizeof(struct sockaddr_in)) == -1 )
	{
		perror("bind");
		return -2;
	}
	
	if( listen(sock, 20) < 0 )
	{
		perror("listen");
		return -3;
	}

	return sock;
}
