#include "main.h"

int main(int argc, const char *argv[])
{
	switch(argc)
	{
		case 1:
			openConfigureFile(DEFAULT_FILE_CONF);
			break;
		case 2:
			if(strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0)
			{
				printHelp();
				exit(EXIT_SUCCESS);
			}
			else
			{
				openConfigureFile(argv[1]);
			}
			break;
	}

	int epollfd = epoll_init();
	server(epollfd);		
	
	exit(EXIT_SUCCESS);
}
