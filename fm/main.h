#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <termios.h>

#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

enum keys{ K_UP, K_DOWN, K_LEFT, K_RIGHT,
           K_ESC, K_ENTER, K_CHAR, K_NULL, K_H};

unsigned int contact_num;
struct termios term, setting;
enum keys key;

extern void press_enter(char *);
extern int file_in_folder(char *, char ***, int *);
extern int comp(const void *a, const void *b);
extern int drawFolder(char *name, int *num, char **);
extern void printHelp();
extern int cmd_tok(char *cmd, char ***tok, int *count);
//******** term.c***********
extern void set_active_color(int a);
extern void set_norm_color(int a);
extern int readkey(enum keys *key);
extern void setupConsole();
//*************************
