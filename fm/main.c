#include "main.h"

extern unsigned int contact_num;
extern struct termios term, setting; 
extern enum keys key;

int main(int argv, char** argc)
{
    int countFiles, isFolder, selectItem = 0;
    char *file, *name, *folder;
    folder = get_current_dir_name(); 
    setupConsole();
    contact_num = 0;
    do
    {
        system("clear");
        printf("Folder: %s\n", folder);
        isFolder = drawFolder(folder, &selectItem, &name);
		readkey(&key);
        switch(key)
        {
            case K_UP:      selectItem--; break;
            case K_DOWN:    selectItem++; break;
            case K_RIGHT: 
			case K_ENTER:
				file =(char*)malloc(strlen(name) + strlen(folder) + 1);
				strcpy(file, folder);
				strcat(file, "/");
				strcat(file, name);
				if(isFolder) 
				{
					folder = file;
				}
				else
				{
					press_enter(file);
				}
			break; 
            case K_LEFT:
                    for(int i = strlen(folder); i != 0 ; i--)
                    {
                    	folder[i] = '\0';
						if(folder[i] == '/')
                        {
							break;
						}
                    }
					selectItem = 0;
                    break;
        }
		free(name);
    }while(key != K_ESC);

    free(folder);
    system("reset");
    return 0;
}
