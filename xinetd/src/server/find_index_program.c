#include "main.h"

int find_index_program(int fd)
{
	for (int i = 0; i < programs_count; i++) 
	{
		if( programs[i].socket == fd )
		{
			return i;
		}
	}

	return -1;
}
