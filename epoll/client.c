#include "main.h"

void client_in(int socket)
{//Send data from client
	char *str = (char*)malloc(sizeof(char) * 300);
	
	printf("%s: ", nickname);
	str = gets(str);
	
	write(socket, nickname, strlen(nickname) + 1);
	write(socket, ": ", 3);
	write(socket, str, strlen(str) + 1);

	//Send data to child
	for(int i = 0; i < MAX_CLIENT; i++)
	{
		if(clients[i].fd != -1)
		{
			write(clients[i].fd, str, strlen(str) + 1);
		}
	}

	free(str);
}

int client_out(int socket)
{//Recived data from server
  	char *msg;
	if( read_msg(socket, msg) == 1)
	{
		printf("%s\n", msg);

		//Send data to child
		for(int i = 0; i < MAX_CLIENT; i++)
		{
			if(clients[i].fd != -1 && clients[i].fd != socket)
			{
				write(socket, nickname, strlen(nickname) + 1);
				write(socket, ": ", 3);
				write(clients[i].fd, msg, strlen(msg) + 1);
			}
		}
		return 1;
	} 
	else
		return 0;
}
