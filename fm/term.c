#include "main.h"

void set_active_color(int a)
{
        if(a)
                write(1,"\033[47m\033[30m",11);
        else
                write(1,"\033[47m\033[32m",11);

}

void set_norm_color(int a)
{
        if(a)
                write(1,"\033[40m\033[37m",11);
        else
                write(1,"\033[40m\033[32m",11);
}

int readkey(enum keys *key)
{
        char ch;
        *key = K_NULL;
        if(!read(0, &ch, 1)) return 0;
        switch (ch)
        {
                case 'h':  *key = K_H; break;
                case '\n': *key = K_ENTER; break;
                case '\033':
                  	*key = K_ESC;
                	if (read(0, &ch, 1) && ch == '[')
                   	{
                		if(!read(0, &ch, 1)) return 1;
                        else switch (ch)
                        {
                            case 'A': *key = K_UP;    break;
                            case 'B': *key = K_DOWN;  break;
                            case 'C': *key = K_RIGHT; break;
                            case 'D': *key = K_LEFT;  break;
                            case '1':
                                    if(!read(0, &ch, 1)) return 0;
                                    else break;
                            default:break;
                        }
                   }
                   break;
                default: *key = K_CHAR; return ch;
        }
        return 2;
}

void setupConsole()
{
    tcgetattr(0, &term);
    tcgetattr(0, &setting);
    setting.c_lflag&=~ICANON; 
	setting.c_lflag&=~ECHO;
    setting.c_lflag&=~ISIG;
    tcsetattr(0,TCSADRAIN,&setting);
}
