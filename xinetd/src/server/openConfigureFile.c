#include "main.h"

int openConfigureFile(const char *filepath)
{
	FILE *file;
	char *program = malloc(MAX_CHAR_BUFFER);
	PROGRAM_PORT pr_cur;
	programs_count = 0;

	if( (file = fopen(filepath, "r")) == NULL )
	{
		printf("File configure not found.\n");
		exit(EXIT_FAILURE);
	}
	
	while( parse_line( file, &pr_cur, program) )
	{
		programs_count++;
		printf("%d: %s\n", pr_cur.port, pr_cur.program);
		free(pr_cur.program);
	}

	programs = malloc(sizeof(PROGRAM_PORT) * programs_count);
	rewind(file);
	programs_count = 0;
	while( parse_line( file, &programs[programs_count++], program ) );
	programs_count--;
	printf("Read %d lines in cfg file\n", programs_count);
	free(program);

	return EXIT_SUCCESS;
}
