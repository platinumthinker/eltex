#include "main.h"

int epoll_init(int sockfd)
{
	struct epoll_event ev;
	int epollfd;
	
//	if( dup2(sockfd, STDOUT_FILENO) == -1 )
//	{
//		perror("Error duplicare fd");
//		return -1;
//	}

	if(( epollfd = epoll_create(MAX_EVENTS)) == -1 )
	{
		perror("epoll create");
		return -1;
	}
	
	ev.events = EPOLLIN;
	ev.data.fd = sockfd;
	if ( epoll_ctl(epollfd, EPOLL_CTL_ADD, sockfd, &ev) == -1 )
	{
		perror("Don't add epoll event");
		return -1;
	}
	
	ev.events = EPOLLIN;
	ev.data.fd = STDIN_FILENO;
	if ( epoll_ctl(epollfd, EPOLL_CTL_ADD, STDIN_FILENO, &ev) == -1 )
	{
		perror("Don't add epoll event");
		return -1;
	}

	return epollfd;
}
