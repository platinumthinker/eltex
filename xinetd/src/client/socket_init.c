#include "main.h"

int client_socket_init(char *ip, char *port)
{
	int sock = 0;
	int inet_pton_ret = 0;
	struct sockaddr_in addr;
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0)
	{
		perror("socket");
		return -1;
	}
	
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(port));
	inet_pton_ret = inet_pton(AF_INET, ip, &addr.sin_addr);
	if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("connect");
		return -2;
	}
	
	return sock;
}
