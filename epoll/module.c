#include "main.h"

int init_modules()
{
    fprintf(log, "Loading modules:\n");
	
	DIR *dir = opendir(MODULES_FOLDER);
    if(!dir)
    {
		perror("Error open module folder!");
		return EXIT_FAILURE;
	}

    struct dirent *entry;
    modules = malloc(sizeof(struct _MODULE) * MAX_MSG_TYPE);
	for (int i = 0; i < MAX_MSG_TYPE; i++) 
	{
		modules[i].send = NULL;
		modules[i].resv = NULL;
	}

    while((entry = readdir(dir)) != NULL)
    {
        char *name = (char*)malloc(strlen(entry->d_name)+strlen(d_name)+1);
        strcpy(name, d_name);
        strcat(name, "/");
        strcat(name, entry->d_name);
    	void *fd = dlopen(name, RTLD_LAZY);
        if(fd != NULL)
        {
            fprintf(log, "Open %s\n", name);
			char (*index_func)(void) = dlsym("index");
			char index = index_func();
			if(modules[index] != NULL)
			{
				fprintf(log, "Warning! Module %s index duplicate and not loading!", name);
				continue;
			}

	        modules[index].send = dlsym(fd, "send");
	        modules[index].resv = dlsym(fd, "resv");	    
        }
    }
    closedir(dir);
    return 1;
}

void unload_modules()
{
	fprintf(log, "Unload modules\n");
	for (int i = 0; i < count_modules; ++i)
	{
		free(modules[i].name);
		free(modules[i].start);
		dlclose(modules[i].fd);	
	}
	free(modules);
	count_modules = 0;	
}
