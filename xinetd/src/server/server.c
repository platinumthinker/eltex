#include "main.h"

void server(int epollfd)
{
	struct epoll_event events[MAX_EVENT];
	struct sockaddr addr;
	int addrlen;
	int count_events, fd;
	for(;;)
	{
		count_events = epoll_wait(epollfd, events, MAX_EVENT, -1);
		if( count_events == -1 )
		{
			perror("epoll_wait in server.c");
			exit(EXIT_FAILURE);
		}
		
		for(int i = 0; i < count_events; i++) 
		{
			if( events[i].data.fd == STDIN_FILENO )
			{
				char buf;
				read(STDIN_FILENO, &buf, 1);
				if(buf == 'Q')
				{
					for (int j = 0; j < programs_count; j++) 
					{
						epoll_ctl(epollfd, EPOLL_CTL_DEL, programs[i].socket,
						NULL);
						close(programs[i].socket);
						free(programs[i].program);
					}
					free(programs);
					printf("Bye bye!\n");
					exit(EXIT_SUCCESS);
				}
			}
			else
			{
				int index = find_index_program(events[i].data.fd);
				//Connect init
				if( index != -1 )
				{
					printf("Connect to server\n");
					fd = accept(programs[index].socket, &addr, &addrlen);
					if( fd == -1 )
					{
						perror("don't accept socket in server.c");
						exit(EXIT_FAILURE);
					}
					
					//Start program
					thread_prog(fd, index);
				}
			}
		}	
	}
}
