#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <sys/epoll.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>

#define MAX_CLIENT 16
#define MAX_EVENT 4
#define MAX_LEN_NICK 30
#define MAX_MSG_TYPE = 256
#define MODULES_FOLDER "module"
#define LOG_FILE "log.txt"

struct _MODULE
{
	int (*send)(void);
	int (*resv)(void);
} *modules;

typedef struct _SERV
{
	struct sockaddr_in addr;
	char nickname[MAX_LEN_NICK];
	int fd;
}SERV;

SERV *clients;
char *nickname;
FILE *logfile;

int client_socket_init2(char *ip, char *port);
int client_socket_init1(struct sockaddr_in addr);
int server_socket_init(char *port);
void client_in(int socket);
int client_out(int socket);
int server(int epollfd ,int socket);
void close_all(int sig);
int reconnect(int socket, int epollfd);
int read_msg(int socket, char* msg);
int connect(int epollfd, int rgc, char **argv);

